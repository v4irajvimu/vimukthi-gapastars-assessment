import React from "react";
import { Button } from "antd";
import { DeleteOutlined } from "@ant-design/icons";

import { INode } from "../interfaces/INode";

interface SelectedCategoryNodesProps {
  nodes: INode[];
  OnToggle: (nodeIds: string) => void;
}

const SelectedCategoryNodes = ({
  nodes,
  OnToggle,
}: SelectedCategoryNodesProps) => {
  const handleOnChange = (nodeId: string) => {
    OnToggle(nodeId);
  };

  return (
    <div>
      {nodes
        .filter((node: INode) => node.checked)
        .map((parentNode) => (
          <ul key={parentNode.id} style={styles.ulStyle}>
            <li style={styles.liStyle}>
              <Button
                type="primary"
                icon={<DeleteOutlined />}
                danger
                style={styles.deleteBtn}
                onClick={() => handleOnChange(parentNode.id)}
              ></Button>
              <span>{parentNode.name}</span>
            </li>

            {parentNode.child_nodes.length > 0 && parentNode.checked && (
              <li>
                <SelectedCategoryNodes
                  nodes={parentNode.child_nodes}
                  OnToggle={OnToggle}
                />
              </li>
            )}
          </ul>
        ))}
    </div>
  );
};

export default SelectedCategoryNodes;

const styles = {
  selectAllLabel: {
    backgroundColor: "#1890FF65",
    padding: "3px",
  },
  ulStyle: {
    listStyleType: "none",
  },
  liStyle: {
    marginBottom: "10px",
  },
  deleteBtn: { marginRight: "10px" },
};
