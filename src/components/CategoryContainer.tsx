import React, { useState, useEffect, FC } from "react";
import { Row, Col, Typography, Button } from "antd";
import { DeleteOutlined } from "@ant-design/icons";

import { INode } from "../interfaces/INode";

import { computeJsonData } from "../util/methods/common_methods";
import { DummyData } from "../resources/dummy-data";

import CategoryNodes from "./CategoryNodes";
import SelectedCategoryNodes from "./SelectedCategoryNodes";

interface CategoryContainerProps {}

const CategoryContainer: FC<CategoryContainerProps> = (
  props: CategoryContainerProps
) => {
  const { Title } = Typography;

  // Nodes Tree Structure
  const [nodes, setNodes] = useState<INode[]>([]);

  useEffect(() => {
    // Compute flat json response to Tree structure.
    const treeStructuredData = computeJsonData(DummyData);
    setNodes(treeStructuredData);
    return () => {
      setNodes([]);
    };
  }, []);

  /** Toggle Checkbox and Category children nodes
   * @param nodeId string Toggles node id
   */
  const handleOnToggle = (nodeId: string) => {
    let tempNodeTree = [...nodes];
    updateNodes(tempNodeTree, nodeId);
    setNodes(tempNodeTree);
  };

  /** Recursively update tree structure
   * @param nodesArray INode[] Tree node object array
   * @param nodeId string Node Id
   */
  const updateNodes = (nodesArray: INode[], nodeId: string) => {
    nodesArray.forEach((nodeItem: INode) => {
      if (nodeItem.id === nodeId) {
        const prevStateChecked = nodeItem.checked;
        nodeItem.checked = !prevStateChecked;

        if (prevStateChecked) {
          // Remove all checked values from child sub tree to depth
          forceUpdateAllChild(nodeItem.child_nodes, false);
        }
      } else if (nodeItem.child_nodes.length > 0) {
        // Call recursive manner
        updateNodes(nodeItem.child_nodes, nodeId);
      }
    });
  };

  //#region START HANDLE SELECT ALL FUNCTIONALITY

  /**
   * Handle Select All checkbox click event
   * @param nodeId string node id
   */
  const handleOnToggleAll = (nodeId: string) => {
    let tempNodeTree = [...nodes];
    updateChildNodes(tempNodeTree, nodeId);
    setNodes(tempNodeTree);
  };

  /**
   * Update All Children nodes on change select all checkbox event accordingly
   * @param nodesArray INode[] Nodes Array
   * @param nodeId string node id
   */
  const updateChildNodes = (nodesArray: INode[], nodeId: string) => {
    nodesArray.forEach((nodeItem: INode) => {
      if (nodeItem.id === nodeId) {
        const prevStateChecked = nodeItem.checked;
        nodeItem.checked = !prevStateChecked;
        forceUpdateAllChild(nodeItem.child_nodes, !prevStateChecked);
      } else if (nodeItem.child_nodes.length > 0) {
        updateChildNodes(nodeItem.child_nodes, nodeId);
      }
    });
  };
  //#endregion  END HANDLE SELECT ALL FUNCTIONALITY

  /** Update checked state of the passes Nodes array and subtree to depth
   * @param nodesArray INode[] Nodes Array
   * @param checked boolean Boolean value to force update.
   */
  const forceUpdateAllChild = (nodesArray: INode[], checked: boolean) => {
    nodesArray.forEach((nodeItem) => {
      nodeItem.checked = checked;
      if (nodeItem.child_nodes.length > 0) {
        forceUpdateAllChild(nodeItem.child_nodes, checked);
      }
    });
  };

  /** Remove all selected categories handler
   */
  const handleRemoveAllSelectedClick = () => {
    let tempNodeTree = [...nodes];
    forceUpdateAllChild(tempNodeTree, false);
    setNodes(tempNodeTree);
  };

  return (
    <Row>
      <Col span={12}>
        <Title style={{ textAlign: "center" }}>Tree View</Title>
        {/* Left side tree structure component */}
        <CategoryNodes
          nodes={nodes}
          OnToggle={handleOnToggle}
          OnToggleAll={handleOnToggleAll}
          parents={[]}
        />
      </Col>
      <Col span={12}>
        <Title style={{ textAlign: "center" }}>Selected Categories</Title>
        {/* Remove All Button. renders only if there selected values */}
        {nodes.filter((node: INode) => node.checked).length > 0 && (
          <div style={styles.btnWrapper}>
            <Button
              type="primary"
              icon={<DeleteOutlined />}
              danger
              style={{ marginLeft: "10px" }}
              onClick={handleRemoveAllSelectedClick}
            >
              Remove All
            </Button>
          </div>
        )}

        {/* Selected Nodes Component */}
        <SelectedCategoryNodes nodes={nodes} OnToggle={handleOnToggle} />
      </Col>
    </Row>
  );
};

export default CategoryContainer;

const styles = {
  btnWrapper: {
    marginBottom: "15px",
  },
};
