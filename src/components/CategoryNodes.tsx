import React from "react";
import { Checkbox } from "antd";

import { INode } from "../interfaces/INode";

interface CategoryNodesProps {
  nodes: INode[];
  OnToggle: (nodeIds: string) => void;
  OnToggleAll: (nodeId: string) => void;
  parents: string[];
}

const CategoryNodes = ({
  nodes,
  OnToggle,
  OnToggleAll,
  parents = [],
}: CategoryNodesProps) => {
  /**  Handle primary checkbox click
   * @param nodeId string Node Id
   */
  const handleOnChange = (nodeId: string) => {
    OnToggle(nodeId);
  };

  /** Handle select all checkbox click
   * @param nodeId string Node Id
   */
  const handleOnToggleAll = (nodeId: string) => {
    OnToggleAll(nodeId);
  };

  /**
   * Check weather particular nodes all subtree nodes checked or not
   * @param nodesArray INode[] Nodes Array
   */
  const checkAllSelected = (nodesArray: INode[]) => {
    // Nodes Stack
    let stack: INode[] = [];
    let node, index;
    // Push nodes to stack
    stack.push(...nodesArray);

    while (stack.length > 0) {
      node = stack.pop();
      if (node && node.checked === false) {
        // Not all chid nodes checked.
        return false;
      } else if (node && node.child_nodes && node.child_nodes.length) {
        for (index = 0; index < node.child_nodes.length; index += 1) {
          stack.push(node.child_nodes[index]);
        }
      }
    }
    return true;
  };

  return (
    <div>
      {nodes.map((parentNode) => (
        <ul key={parentNode.id} style={styles.ulStyle}>
          <li style={styles.liStyle}>
            <Checkbox
              checked={parentNode.checked}
              onChange={() => handleOnChange(parentNode.id)}
            >
              {parentNode.name}
            </Checkbox>
            {parentNode.parent !== "0" && parentNode.child_nodes.length > 0 && (
              <span style={styles.selectAllLabel}>
                <Checkbox
                  checked={checkAllSelected(parentNode.child_nodes)}
                  onChange={() => handleOnToggleAll(parentNode.id)}
                >
                  Select All
                </Checkbox>
              </span>
            )}
          </li>

          {parentNode.child_nodes.length > 0 && parentNode.checked && (
            <li>
              <CategoryNodes
                nodes={parentNode.child_nodes}
                OnToggle={OnToggle}
                OnToggleAll={OnToggleAll}
                parents={[...parents, parentNode.id]}
              />
            </li>
          )}
        </ul>
      ))}
    </div>
  );
};

export default CategoryNodes;

const styles = {
  selectAllLabel: {
    backgroundColor: "#1890FF65",
    padding: "3px",
  },
  ulStyle: {
    listStyleType: "none",
  },
  liStyle: {
    marginBottom: "10px",
  },
};
