import React, { FC } from "react";
import "./App.css";
import { Layout } from "antd";
import CategoryContainer from "./components/CategoryContainer";

const App: FC = () => {
  const { Content } = Layout;
  return (
    <Layout>
      <Content>
        <CategoryContainer />
      </Content>
    </Layout>
  );
};

export default App;
