import { ICategory } from "./ICategory";

export interface INode extends ICategory {
  child_nodes: INode[];
  checked: boolean;
}
