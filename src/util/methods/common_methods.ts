import { ICategory } from "../../interfaces/ICategory";
import { INode } from "../../interfaces/INode";

/**
 * Get computes tree structure of the categories response
 * @param categories ICategory[] Categories data list
 * @param id string group by id - parent_id
 */
export const computeJsonData = (categories: ICategory[], id: string = "0") => {
  if (categories.length === 0) return [];
  const treeNodes: INode[] = categories
    .filter((category: ICategory) => category.parent === id)
    .map((category: ICategory) => {
      return {
        ...category,
        child_nodes: computeJsonData(categories, category.id),
        checked: false,
      };
    });

  return treeNodes;
};
