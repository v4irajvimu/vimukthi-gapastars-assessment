# Vimukthi Jayasinghe - Gapstars | Otrium Challenge | ReactJS

## Clone the Project

### `git clone https://gitlab.com/v4irajvimu/vimukthi-gapastars-assessment.git`

## Change to the project directory

### `cd vimukthi-gapastars-assessment`

## Install dependencies

### `yarn`

## Run the project

### `yarn start`
